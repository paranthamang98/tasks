// First-chart

let ctx = document.getElementById("chart").getContext('2d');
var gradientBkgrd = ctx.createLinearGradient(0, 50, 0, 200);
gradientBkgrd.addColorStop(0, "#E1BEF3");
gradientBkgrd.addColorStop(1, "#F9C8BF");

var chart = new Chart(ctx , {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr","May","Jun","Jul","Aug","Sep", "Oct", "Nov", "Dec"],
        datasets: [{
            label: "Income",
            backgroundColor: gradientBkgrd,
            data: [5500, 250, 10000, 600, 14000, 1500, 7000,2000, 14000, 1500, 7000,2000],
            pointBorderColor: "rgba(255,255,255,0)",
            pointBackgroundColor: "rgba(255,255,255,0)",
            pointBorderWidth: 0,
            pointHoverRadius: 8,
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 4,
            pointRadius: 1,
            borderWidth: 0,
            pointHitRadius: 16,
            
        }]
    },
    options: {
      tooltips: {
        backgroundColor:'#fff',
        displayColors:false,
           titleFontColor: '#000',
        bodyFontColor: '#000'
        
        },      
      legend: {
            display: false
      },
        scales: {
            xAxes: [{
                gridLines: {
                    display:false,
                    drawBorder: false,
                    
                }
            }],
            yAxes: [{
              gridLines: {
                drawBorder: false,
                    display:false
                },
                ticks: {
            display: false
        }
            }],
        }
    }
});


// second chart

let cct = document.getElementById("charttwo").getContext('2d');
var chart = new Chart(cct, {
    type: 'line',
    data: {
        labels: ["Jan", "Feb", "Mar", "Apr","May","Jun","Jul","Aug","Sep", "Oct", "Nov", "Dec"],
        datasets: [{
            label: "Income",
            backgroundColor: "#C5ACD0 ",
            data: [5500, 250, 10000, 600, 14000, 1500, 7000,2000, 14000, 1500, 7000,2000],
            pointBorderColor: "rgba(255,255,255,0)",
            pointBackgroundColor: "rgba(255,255,255,0)",
            pointBorderWidth: 0,
            pointHoverRadius: 8,
            pointHoverBorderColor: "rgba(220,220,220,1)",
            pointHoverBorderWidth: 4,
            pointRadius: 1,
            borderWidth: 0,
            pointHitRadius: 16,
            
        }]
    },
    options: {
      tooltips: {
        backgroundColor:'#fff',
        displayColors:false,
           titleFontColor: '#000',
        bodyFontColor: '#000'
        
        },      
      legend: {
            display: false
      },
        scales: {
            xAxes: [{
                gridLines: {
                    display:false,
                    drawBorder: false,
                    
                }
            }],
            yAxes: [{
              gridLines: {
                drawBorder: false,
                    display:false
                },
                ticks: {
            display: false
        }
            }],
        }
    }
});





// jQuery 

$(document).ready(function(){
    $(".side-maenu-content").click(function(){            
        $(this).toggleClass("active-bg");
        $(this).siblings().removeClass("active-bg");             
    });  
    
    $(".nav-bar").click(function(){          
    $(this).parents(".page-wapper").toggleClass("open");
    });

    $(".close-icon").click(function(){
        $(this).parents(".page-wapper").removeClass("open");
    });
    
})